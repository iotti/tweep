class TweetSerializer
  include FastJsonapi::ObjectSerializer
  attributes :followers_count, :screen_name, :profile_link, :created_at, :link, :retweet_count, :text, :favorite_count
end
